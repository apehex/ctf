# CTF

> :triangular_flag_on_post: CTF writeups

A bunch of scripts & notes, organised by platform / theme / challenge.

## Development

Contributions welcome! Read the [contribution guidelines](CONTRIBUTING.md) first.

## Credits

This project was initially created with [Cookiecutter][cookiecutter] and the custom [cookiecutter-git][cookiecutter-git] :cookie:

## License

> Free software: MIT

See [LICENSE](LICENSE)

[cookiecutter]: https://github.com/audreyr/cookiecutter
[cookiecutter-git]: https://github.com/apehex/cookiecutter-git
